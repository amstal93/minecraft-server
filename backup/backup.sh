#!/bin/bash
# run as sudo 

BACKUP_TIME=`date '+%Y%m%d_%H%M%S'`
tar -cvf backup_$BACKUP_TIME.tar.gz  /var/lib/docker/volumes/minecraft_data/
