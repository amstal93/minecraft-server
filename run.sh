#!/bin/bash

docker run --name minecraft-srv \
--restart=always \
-p 25565:25565 \
-p 25575:25575 \
--mount source=minecraft_data,target=/minecraft \
-v /home/pi/minecraft-server/server.properties:/minecraft/server.properties \
-v /home/pi/minecraft-server/ops.json:/minecraft/ops.json \
-d minecraft-srv-rpi