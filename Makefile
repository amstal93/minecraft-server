
.DEFAULT_GOAL := server-image

REV=latest

server-image:
	docker build -t minecraft-srv --rm --build-arg REV=${REV} .

build-jar:
	docker build -t minecraft-srv --rm --build-arg REV=${REV} . && \
	docker run --name tmp-minecraft-srv minecraft-srv sh -c "tar -cvf minecraft.tar.gz /minecraft" && \
	docker cp tmp-minecraft-srv:/minecraft/minecraft.tar.gz . && \
	docker rm -f tmp-minecraft-srv

